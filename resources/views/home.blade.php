@extends('layouts.app')
@section('content')
<div class="row">
   <div class="col-lg-12 margin-tb">
      <div class="pull-left">
         <h2>Contacts</h2>
      </div>
   </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
   <p>{{ $message }}</p>
</div>
@endif
<table class="table table-bordered">
   <tr>
      <th>No</th>
      <th>Name</th>
      <th>Contact</th>
      <th>Email</th>
   </tr>
   @foreach ($contacts as $contact)
   <tr>
      <td>{{ ++$i }}</td>
      <td>{{ $contact->name }}</td>
      <td>{{ $contact->contact }}</td>
      <td>{{ $contact->email }}</td>
   </tr>
   @endforeach
</table>
{!! $contacts->links() !!}
@endsection