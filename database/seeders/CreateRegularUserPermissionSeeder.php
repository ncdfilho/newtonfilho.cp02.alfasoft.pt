<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateRegularUserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        $role = Role::create(['name' => 'User']);
     
        $permissions = [
           'contact-list'
        ];
   
        $role->syncPermissions($permissions);

    }
}
