# Alfasoft Test - Laravel.

## Installation

### Requirements

https://laravel.com/docs/8.x

# Clone Repo

```
git clone https://ncdfilho@bitbucket.org/ncdfilho/newtonfilho.cp02.alfasoft.pt.git
```

### Generate symbolic link to Storage
```
php artisan storage:link
```

### Run the command below to initialize.
```
php artisan alfasoft:initialize --seed
```

## Modules

- Auth
  - Login
  - Register
  - Remember Password

- Contact
    - List contacts
    - Create contacts
    - Update contacts
    - Delete contacts

- Roles & Permission
    - List roles
    - Create roles and assign permissions
    - Update role and permissions
    - Users list who use the role
    - Delete role

---

> By [@ncdfilho](https://www.linkedin.com/in/ncdfilho/)
